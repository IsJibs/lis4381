# LIS4381
## Victor Ogden Villar

### Project #1 Requirements:

1. Screenshot of running application’s first user interface
2. Screenshot of running application’s second user interface
3. Skill sets 7 - 9

#### *Screenshot P1 App Screen 1*:

![P1 Screen1](img/screen1.PNG)

#### *Screenshot P1 App Screen 2*:

![P1 Screen2](img/screen2.PNG)

#### *Skill Set 7*

![SkillSet7](img/skillset7.PNG)

#### *Skill Set 8*

![SkillSet8](img/skillset8.PNG)

#### *Skill Set 9*

![SkillSet9](img/SkillSet9.PNG)