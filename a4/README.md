# LIS4381

## Victor Ogden Villar

### Assignment #2 Requirements:

1. Bruschetta recipe app
2. Change background and text color
3. Skill sets 1 - 3

#### Assignment Screenshots:

*App Screen 1*:

![Screen 1](img/screen1.png)

*App Screen 2*

![Screen 2](img/screen2.png)

*Skill Set 1*

![Even or Odd](img/skillset1.png)

*Skill Set 2*

![Prime](img/skillset2.png)

*Skill Set 3*

![Arrays and Loops](img/skillset3.png)