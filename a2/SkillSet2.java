import java.util.Scanner;

public class SkillSet2 {
	private static Scanner sc;
	public static void main(String[] args) 
	{
		int number1, number2;
		sc = new Scanner(System.in);
		
		System.out.print("Program evaluates largest of two integers.\n");
		System.out.print("Note: Program does *not* check for non-numeric characters or non-numeric values.\n\n");
		
		System.out.print("Enter first integer: ");
		number1 = sc.nextInt();	
		
		System.out.print("Enter second integer: ");
		number2 = sc.nextInt();	
		
		if(number1 > number2) 
	    {
			System.out.println("\n" + number1 + " is larger than " + number2);          
	    } 
	    else if (number2 > number1)
	    { 
	    	System.out.println("\n" + number2 + " is larger than " + number1);        
	    } 
	    else 
	    {
	    	System.out.println("\n Integers are Equal");
	    }		
	}	
}