public class SkillSet3
{

   public static void main(String[] args)
   
   {
      System.out.print("Program loops through array of strings.\n");
      System.out.print("Use following values: dog, cat, bird, fish, insect.\n");
      System.out.println("Use following loop structures: for, enhanced for, while, do...while.\n");
      System.out.println("Note: Pretest loops: for, enhanced for, while. Posttest loop: do...while.\n");

      String[] list = {"dog", "cat", "bird", "fish", "insect"};

      int size = list.length;
      System.out.println("for loop:");
      for (int i = 0; i < size; i++)
      {
        System.out.println(list[i]);
      }
      System.out.println(" ");

      System.out.println("Enhanced for loop:");
      for (String l: list)
      {
        System.out.println(l);
      }
      System.out.println(" ");

      System.out.println("while loop:");
      int count = 0;
      while (count < size)
      {
        System.out.println(list[count]);
        count++;
      }
      System.out.println(" ");

      System.out.println("do...while loop:");
      int count2 = 0;
      do
      {
        System.out.println(list[count2]);
        count2++;
      } while (count2 < size);
      System.out.println(" ");
   }
}