# LIS4381

## Victor Ogden Villar

### Assignment #2 Requirements:

1. Bruschetta recipe app
2. Change background and text color
3. Skill sets 1 - 3

#### Assignment Screenshots:

*App Screen 1*:

![Screen1](img/screen1.PNG)

*App Screen 2*

![Screen2](img/screen2.PNG)

*Skill Set 1*

![SkillSet1](img/skillset1.PNG)

*Skill Set 2*

![SkillSet2](img/skillset2.PNG)

*Skill Set 3*

![SkillSet3](img/skillset3.PNG)