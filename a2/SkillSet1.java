import java.util.Scanner;

class SkillSet1
{
  public static void main(String args[])
  {
    int num;
    System.out.println("Program evaluates integers as even or odd.");
    System.out.println("Note: Program does *not* check for non-numeric characters.\n");
    System.out.println("Enter Integer:");

    Scanner input = new Scanner(System.in);
    num = input.nextInt();

    if ( num % 2 == 0 )
        System.out.println(num + " is an even number.");
     else
        System.out.println(num + " is an odd number.");
  }
}