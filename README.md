> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# lis4381

## Victor Ogden Villar

### lis4381 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
	- Install AMPPS
	- Install JDK
	- Install Android Studio
	- Define Git Commands

2. [A2 README.md](a2/README.md "My A2 README.md file")
	- Bruschetta recipe app
	- Change background and text color
	- Skill sets 1 - 3
    
3. [A3 README.md](a3/README.md "My A3 README.md file")

	- Screenshot of ERD
	- Screenshot of running application’s first user interface
	- Screenshot of running application’s second user interface
	- Links to a3.mwb and a3.sql
	- Skill sets 4 - 6
    
4. [A4 README.md](a4/README.md "My A4 README.md file")

5. [A5 README.md](a5/README.md "My A5 README.md file")

6. [P1 README.md](p1/README.md "My P1 README.md file")

	- Screenshot of running application’s first user interface
	- Screenshot of running application’s second user interface
	- Skill sets 7 - 9
    
7. [P2 README.md](p2/README.md "My P2 README.md file")
