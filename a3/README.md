# LIS4381
## Victor Ogden Villar

### Assignment #3 Requirements:

1. Screenshot of ERD
2. Screenshot of running application’s first user interface
3. Screenshot of running application’s second user interface
4. Links to a3.mwb and a3.sql

#### *Screenshot A3 ERD*:

![A3 ERD](img/a3.png)

#### *Screenshot A3 App Screen 1*:

![A3 Screen1](img/screen1.PNG)

#### *Screenshot A3 App Screen 2*:

![A3 Screen2](img/screen2.PNG)

#### *Skill Set 4*

![SkillSet4](img/skillset4.PNG)

#### *Skill Set 5*

![SkillSet5](img/skillset5.PNG)

#### *Skill Set 6*

![SkillSet6](img/skillset6.PNG)

#### *A3 docs: a3.mwb and a3.sql*:

[A3 MWB File](docs/a3.mwb)
[A3 SQL File](docs/a3.sql)